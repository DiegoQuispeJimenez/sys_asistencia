<?php
    include_once 'empleado.php';

    class ApiEmpleados{

        function getById($codEmpleado){
            $empleado = new Empleado();
            $empleados = array();
            $empleados["Empleado"] = array();

            $res = $empleado->obtenerEmpleado($codEmpleado);

            if($res->rowCount() >= 1){
                $row = $res->fetch();
                $item=array(
                    "Cod_Empleado" => $row['CODEMPLEADO'],
                    "Apellidos" => $row['APELLIDOS'],
                    "Nombres" => $row['NOMBRES']
                );
                array_push($empleados["Empleado"], $item);
                //$this->printJSON($empleados);

                $insertMarca = $empleado->registrarMarcacion($codEmpleado);
                echo '1';

            }else{
                    echo 'Cod_Empleado: '. $codEmpleado . '<br>';
                    echo json_encode(array('mensaje' => 'No hay elementos'));
                }
            }

        function error($mensaje){
            echo json_encode(array('mensaje' => $mensaje)); 
        }

        function printJSON($array){
            echo '<code>'.json_encode($array).'</code>';
        }

    }

?>

<?php

	include_once 'db.php';

	class Empleado extends DB{
	    
	    function obtenerEmpleados(){
	        $query = $this->connect()->query("SELECT tbl_empleados.codEmpleado AS CODEMPLEADO,apellidos AS APELLIDOS,nombres AS NOMBRES FROM tbl_empleados INNER JOIN tbl_personas ON tbl_empleados.codEmpleado = tbl_personas.codEmpleado WHERE tbl_empleados.estado = 'O'");
	        return $query;
	    }

	    function obtenerEmpleado($codEmpleado){
	        $query = $this->connect()->prepare("SELECT tbl_empleados.codEmpleado AS CODEMPLEADO,apellidos AS APELLIDOS,nombres AS NOMBRES FROM tbl_empleados INNER JOIN tbl_personas ON tbl_empleados.codEmpleado = tbl_personas.codEmpleado WHERE tbl_empleados.estado = 'O' AND tbl_empleados.codEmpleado = :codEmpleado");
	        $query->execute(array("codEmpleado" => $codEmpleado));
	        return $query;
	    }

	     function registrarMarcacion($codEmpleado){

	        $query = $this->connect()->prepare("INSERT INTO tbl_marcaciones(fechaHora,codEmpleado,justificacion,comodin,estado) VALUES(NOW(),:codEmpleado,'','','O')");

	        $query->execute([
	        	'codEmpleado' => $codEmpleado
	   
	    ]);
	        return $query;
	    }
	}
?>

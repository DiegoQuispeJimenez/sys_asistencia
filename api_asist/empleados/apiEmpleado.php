<?php
    include_once 'empleado.php';

    class ApiEmpleados{
        /*function getAll(){
            $audio = new Audio();
            $audios = array();
            $audios["Grabacion"] = array();
            $res = $audio->obtenerAudios();
            if($res->rowCount()){
                while ($row = $res->fetch(PDO::FETCH_ASSOC)){

                    $link = "http://172.16.96.253/grabaciones/";
                    $audioPath = $row['recordingfile'];
                    $audioLink = $link . $audioPath;

                    $item=array(
                        "uniqueid" => $row['uniqueid'],
                        "audio" => $audioLink,
                    );
                    array_push($audios["Grabacion"], $item);
                }
            
                $this->printJSON($audios);
            }else{
                echo json_encode(array('mensaje' => 'No hay elementos'));
            }
        }*/

        function getById($codEmpleado){
            $empleado = new Empleado();
            $empleados = array();
            $empleados["Empleado"] = array();

            $res = $empleado->obtenerEmpleado($codEmpleado);

            if($res->rowCount() >= 1){
                $row = $res->fetch();
                $item=array(
                    "Cod_Empleado" => $row['CODEMPLEADO'],
                    "Apellidos" => $row['APELLIDOS'],
                    "Nombres" => $row['NOMBRES']
                );
                array_push($empleados["Empleado"], $item);
                $this->printJSON($empleados);
            }else{
                    echo 'Cod_Empleado: '. $codEmpleado . '<br>';
                    echo json_encode(array('mensaje' => 'No hay elementos'));
                }
            }

        function error($mensaje){
            echo json_encode(array('mensaje' => $mensaje)); 
        }

        function printJSON($array){
            echo '<code>'.json_encode($array).'</code>';
        }
    }
?>

/* RELOJ */
var $hands = $('#liveclock div.hand')
window.requestAnimationFrame = window.requestAnimationFrame

                               || window.mozRequestAnimationFrame

                               || window.webkitRequestAnimationFrame

                               || window.msRequestAnimationFrame

                               || function(f){setTimeout(f, 60)}

function updateclock(){
	var curdate = new Date()
	var hour_as_degree = ( curdate.getHours() + curdate.getMinutes()/60 ) / 12 * 360
	var minute_as_degree = curdate.getMinutes() / 60 * 360
	var second_as_degree = ( curdate.getSeconds() + curdate.getMilliseconds()/1000 ) /60 * 360
	$hands.filter('.hour').css({transform: 'rotate(' + hour_as_degree + 'deg)' })
	$hands.filter('.minute').css({transform: 'rotate(' + minute_as_degree + 'deg)' })
	$hands.filter('.second').css({transform: 'rotate(' + second_as_degree + 'deg)' })
	requestAnimationFrame(updateclock)
}
requestAnimationFrame(updateclock)
/* FIN RELOJ */

/* FECHA */
var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
var diasSemana = new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
var f = new Date();
document.getElementById('fecha').innerHTML=(diasSemana[f.getDay()] + ", " + f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear());
/* FIN FECHA */

/* TIPPY JS */
  tippy('btn-admin', {
  	delay: 100,
  	arrow: true,
  	arrowType: 'round',
  	size: 'large',
  	duration: 500,
  	animation: 'scale'
  })
/* FIN TIPPY JS */


/* AJAX */
/*$(document).ready(function(){
  $('#btnguardar').click(function(){
    var datos=$('#frmajax').serialize();
    $.ajax({
      type:"POST",
      url:"insertar.php",
      data:datos,
      success:function(r){
        if(r==1){
          alert("agregado con exito");
        }else{
            alert("Fallo el server");
        }
      }
    });
  return false;
  });
});*/
